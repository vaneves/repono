<?php
/*
 * Copyright (c) 2011, Valdirene da Cruz Neves Júnior <linkinsystem666@gmail.com>
 * All rights reserved.
 */


/**
 * As rotas são para reescrita de URL. Veja um exemplo:
 * Route::add('^([\d]+)-([a-z0-9\-]+)$','home/view/$1/$2');
 * 
 * Também é possível criar prefixos. Veja um exemplo:
 * Route::prefix('admin');
 */

Route::add('^search/?$','component/search');

Route::add('^login/?$','user/login');
Route::add('^forgot/?$','user/forgot');
Route::add('^register/?$','user/register');
Route::add('^logout/?$','user/logout');

Route::add('^about/?$','repono/about');
Route::add('^doc/?$','repono/doc');