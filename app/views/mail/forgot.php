<div style="padding: 10px;">
	<h2>Recuperação de Senha</h2>
	<div style="background: #F5F5F5; border: 1px solid #DDD; border-radius: 10px; padding: 10px;">
		<p>Olá <?= $name ?>,</p>
		<p>
			recebemos uma solicitação de recuperação de seus dados cadastrais. 
			Para sua segurança o sistema gerou uma nova senha, que segue abaixo:
		</p>
		<p>
			<b>Usuário: </b> <?= $name ?>
		</p>
		<p>
			<b>Senha: </b> <?= $password ?>
		</p>
		<p>Atenciosamente,</p>
		<p>Equipe do Repono.</p>
	</div>
</div>