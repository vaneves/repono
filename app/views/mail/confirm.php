<div style="padding: 10px;">
	<h2>Confirmação de Cadastro</h2>
	<div style="background: #F5F5F5; border: 1px solid #DDD; border-radius: 10px; padding: 10px;">
		<p>Olá <?= $name ?>,</p>
		<p>
			seja bem-vindo ao Repono. Por favor, confirme seu cadastro para começar a utilizar essa incrível ferramenta:
		</p>
		<p>
			<a href="<?= $link ?>"><?= $link ?></a>
		</p>
		<p>Atenciosamente,</p>
		<p>Equipe do Repono.</p>
	</div>
</div>