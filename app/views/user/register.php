<div class="row-fluid">
	<div class="span7">
		<div class="well well-large">
			<form method="post" action="">
				<fieldset>
					<legend>Cadatrar</legend>
					<?= FLASH ?>
					
					<?= BForm::input('Usuário', 'Username', $model->Username, 'span12') ?>
					<?= BForm::input('E-mail', 'Email', $model->Email, 'span12') ?>
					<div class="row-fluid">
						<div class="span6">
							<?= BForm::password('Senha', 'Password', null, 'span12') ?>
						</div>
						<div class="span6">
							<?= BForm::password('Confirmar Senha', 'Confirm', null, 'span12') ?>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span4">
							<button type="submit" class="btn btn-primary btn-block">Cadastrar</button>
						</div>
						<div class="span8">
							<label class="checkbox"><input type="checkbox" name="Term" value="accepted"> Concordo com os termos de uso</label>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
	<div class="span5">
		<h4>Termos de Uso</h4>
		<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>
	</div>
</div>