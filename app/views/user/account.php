<div class="row-fluid">
	<div class="span7">
		<div class="well well-large">
			<form method="post" action="">
				<fieldset>
					<legend>Minha Conta</legend>
					<?= FLASH ?>
					<?= BForm::input('Senha Antiga', 'Old', null, 'span10') ?>
					<?= BForm::input('Nova Senha', 'Password', null, 'span10') ?>
					<?= BForm::input('Confirmar Senha', 'Confirm', null, 'span10') ?>
					<button type="submit" class="btn btn-primary">Salvar</button>
				</fieldset>
			</form>
		</div>
	</div>
</div>