<div class="row-fluid">
	<div class="span7">
		<div class="well well-large">
			<form method="post" action="">
				<fieldset>
					<legend>Entrar</legend>
					<?= FLASH ?>
					
					<?= BForm::input('Usuário', 'Username', null, 'span12') ?>
					<?= BForm::password('Senha', 'Password', null, 'span12') ?>

					<button type="submit" class="btn btn-primary">Entrar</button>
					<a href="~/forgot" class="pull-right">Esqueci minha senha</a>
				</fieldset>
			</form>
		</div>
	</div>
</div>