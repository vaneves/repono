<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>Repono</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Vaneves">

		<link href="~/css/all.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<div class="masthead">
				<form class="pull-right" method="get" action="~/search/">
					<div class="input-append">
						<input class="span3" id="appendedInputButton" type="text" name="q" placeholder="Pesquisar componente" value="<?= Request::get('q') ?>">
						<button class="btn" type="submit"><i class="icon-search"></i></button>
					</div>
				</form>
				<h1 class="muted">Repono</h1>
				<div class="clearfix"></div>
				<div class="navbar">
					<div class="navbar-inner">
						<div class="container">
							<ul class="nav">
								<li><a href="~/">Início</a></li>
								<?php if(Auth::isLogged()): ?>
								<li><a href="~/component/my">Meus Componentes</a></li>
								<?php endif ?>
								<li><a href="~/component/submit">Enviar Componente</a></li>
								<li><a href="~/middleware">Baixar Middleware</a></li>
								<li><a href="~/doc">Documentação</a></li>
								<li><a href="~/about">Sobre</a></li>
							</ul>
							<ul class="nav pull-right">
								<?php if(!Auth::isLogged()): ?>
								<li><a href="~/login">Entrar</a></li>
								<li><a href="~/register">Cadastrar</a></li>
								<?php else: ?>
								<li><a href="~/user/account">Minha Conta</a></li>
								<li><a href="~/logout">Sair</a></li>
								<?php endif ?>
							</ul>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<?= CONTENT ?>
			<hr>
			<div class="footer">
				<p class="muted">&copy; <?= date('Y') ?> <b>Vaneves</b></p>
			</div>
		</div>
		<script src="~/js/all.js"></script>
	</body>
</html>
