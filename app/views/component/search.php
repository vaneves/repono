<div class="row-fluid">
	<?php foreach($model->Data as $m): ?>
	<div class="media">
		<a class="pull-left span2" href="~/component/view/<?= $m->Name ?>/<?= $m->Version ?>">
			<img src="~/upload/img/<?= $m->Image ?>" alt="<?= $m->Title ?>">
		</a>
		<div class="media-body">
			<h2 class="media-heading">
				<a href="~/component/view/<?= $m->Name ?>/<?= $m->Version ?>">
					<?= $m->Title ?> 
					<small>(<?= $m->Version ?>)</small>
				</a>
			</h2>
			<p>
				<?= substr(utf8_encode($m->Description), 0, 250) ?>
			</p>
			<p>
				<span class="label label-info"><i class="icon-white icon-download"></i> 891</span>
			</p>
		</div>
	</div>
	<?php endforeach ?>
</div>
<?= Pagination::create('search/', $model->Count, $p) ?>