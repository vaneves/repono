<div class="row-fluid">
	<div class="span7">
		<div class="well well-large">
			<form method="post" action="" enctype="multipart/form-data">
				<fieldset>
					<legend>Enviar Componente</legend>
					<?= FLASH ?>
					<div class="control-group">
						<label class="control-label" for="Image">Imagem</label>
						<div class="controls">
							<input type="file" name="Image" id="Image" placeholder="Imagem" class="span12">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" for="File">Arquivo</label>
						<div class="controls">
							<input type="file" name="File" id="File" placeholder="Arquivo" class="span12">
						</div>
					</div>

					<button type="submit" class="btn btn-primary">Enviar</button>
				</fieldset>
			</form>
		</div>
	</div>
</div>