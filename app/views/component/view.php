<div class="row-fluid">
	<div class="span3">
		<div class="thumbnail">
			<img src="~/upload/img/<?= $model->Image ?>" alt="<?= $model->Title ?>">
			<div class="caption">
				<h3>Downloads</h3>
				<table class="table table-striped">
					<tbody>
						<tr>
							<td><b>Total</b></td>
							<td><?= $count_total ?></td>
						</tr>
						<tr>
							<td><b>Mês</b></td>
							<td><?= $count_month ?></td>
						</tr>
						<tr>
							<td><b>Hoje</b></td>
							<td><?= $count_day ?></td>
						</tr>
					</tbody>
				</table>
				<a href="~/component/download/<?= $model->Name ?>/<?= $model->Version ?>" class="btn btn-primary btn-block">Download</a>
			</div>
		</div>
	</div>
	<div class="span9">
		<h2><?= $model->Title ?> <small>(<?= $model->Version ?>)</small></h2>
		<p>
			<?= htmlentities(utf8_encode($model->Description)) ?>
		</p>

		<h3>Autores</h3>
		<div class="row-fluid">
			<ul class="thumbnails">
				<?php $authors = json_decode($model->Authors) ?>
				<?php foreach($authors as $a): ?>
				<li class="span1">
					<div class="thumbnail">
						<img src="http://www.gravatar.com/avatar/<?= md5(strtolower(trim($a->email))) ?>?s=44&d=mm" alt="<?= $a->name ?>" title="<?= $a->name ?>">
					</div>
				</li>
				<?php endforeach ?>
			</ul>
		</div>
		<?php $requires = json_decode($model->Requires) ?>
			<?php if(count($requires)): ?>
			<h3>Dependências</h3>
			<ul>
				<?php foreach($requires as $r): ?>
				<li><a href="~/component/view/<?= $r->name ?>/<?= $r->version ?>"><?= $r->name ?></a> <?= $r->version ?></li>
				<?php endforeach ?>
			</ul>
		<?php endif ?>

		<h3>Histórico de Versões</h3>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Versão</th>
					<th>Data</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($versions as $v): ?>
				<tr>
					<td><a href="~/component/view/<?= $v->Name ?>/<?= $v->Version ?>"><?= $v->Title . ' ' . $v->Version ?></a></td>
					<td><?= date('d/m/Y', $v->CreateDate) ?></td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>