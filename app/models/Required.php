<?php
/**
 * @Entity("required")
 */
class Required extends Model
{
	/**
	 * @Column(Type="Int",Key="Primary")
	 */
	public $Id;
	
	/**
	 * @Column(Type="Int")
	 */
	public $ComponentId;
	
	/**
	 * @Column(Type="String")
	 */
	public $RequiredName;
	
	/**
	 * @Column(Type="String")
	 */
	public $Version;
}