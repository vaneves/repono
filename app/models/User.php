<?php
/**
 * @Entity("user")
 */
class User extends Model
{
	/**
	 * @AutoGenerated()
	 * @Column(Type="Int",Key="Primary")
	 */
	public $Id;
	
	/**
	 * @Column(Type="String")
	 * @Regex(Pattern="^([a-zA-Z0-9]+){3,16}$",Message="Use apenas letras e números com no mínimo 3 caracteres.")
	 */
	public $Username;
	
	/**
	 * @Column(Type="String")
	 */
	public $Email;
	
	/**
	 * @Column(Type="String")
	 */
	public $Password;
	
	/**
	 * @Column(Type="Boolean")
	 */
	public $IsApproved = false;
	
	/**
	 * @Column(Type="Int")
	 */
	public $CreateDate;
	
	public static function get_by_name($username)
	{
		$db = Database::factory();
		return $db->User->single('Username = ?', $username);
	}
	
	public static function get_by_email($email)
	{
		$db = Database::factory();
		return $db->User->single('Email = ?', $email);
	}
}