<?php
class UserController extends Controller
{
	public function register()
	{
		$user = new User();
		if(Request::isPost())
		{
			try 
			{
				$user = $this->_data($user);
				if(Request::post('Term'))
				{
					if(!User::get_by_name($user->Username))
					{
						if($user->Password == $this->_data()->Confirm)
						{
							$user->Password = md5($user->Password);
							$user->CreateDate = time();

							$user->save();
							$this->_flash('alert', 'Obrigado por efetuar seu cadastro, por favor, confirme sua conta por e-mail.');
							
							$mail = new Mail();
							$mail->setSubject('Confirmação de Cadastro');
							$mail->setFrom('nao-responda@vaneves.com', 'Repono');
							$mail->addTo($user->Email, $user->Username);
							$mail->setTemplate('mail', 'confirm', array('name' => $user->Username, 'link' => Request::getSite() . 'user/confirm/' . Security::encrypt($user->Id, Config::get('my_key'))));

							$response = $mail->send();
						}
						else
						{
							$this->_flash('alert alert-error', 'Os campos "Senha" e "Confirmar Senha" não estão iguais.');
						}
					}
					else
					{
						$this->_flash('alert alert-error', 'O "Usuário" já está em uso.');
					}
				}
				else
				{
					$this->_flash('alert alert-error', 'Você deve aceitar os termos de uso.');
				}
			} 
			catch (ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch (Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível criar sua conta.');
			}
		}
		return $this->_view($user);
	}
	
	public function confirm($code)
	{
		$id = Security::decrypt(base64_decode($code), Config::get('my_key'));
		$user = User::get($id);
		if($user)
		{
			try
			{
				$user->IsApproved = true;
				$user->save();
				
				$this->_flash('alert', 'Obrigado por confirmar seu cadastro, por favor, faça seu login.');
			}
			catch (Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível confirmar sua conta.');
			}
		}
		else
		{
			$this->_flash('alert alert-error', 'Código inválido.');
		}
		return $this->_snippet('clean');
	}
	
	public function forgot()
	{
		if(Request::isPost())
		{
			$user  = User::get_by_email($this->_data()->Email);
			if($user)
			{
				try
				{
					$passwd = new_passwd();
					$user->Password = md5($passwd);
					$user->save();
					
					$mail = new Mail();
					$mail->setSubject('Recuperação de Senha');
					$mail->setFrom('nao-responda@vaneves.com', 'Repono');
					$mail->addTo($user->Email, $user->Username);
					$mail->setTemplate('mail', 'forgot', array('name' => $user->Username, 'password' => $passwd));

					$response = $mail->send();
					
					$this->_flash('alert', 'Sua nova senha foi enviada por e-mail.');
				}
				catch(Exception $e)
				{
					$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar recuperar sua senha.');
				}
			}
			else
			{
				$this->_flash('alert alert-error', 'E-mail não cadastrado.');
			}
		}
		return $this->_view($this->_data());
	}
	
	/** @Auth("user") */
	public function account()
	{
		if(Request::isPost())
		{
			try 
			{
				$user = User::get(Session::get('user')->Id);
				if($user)
				{
					$data = $this->_data();
					if($user->Password == $data->Old)
					{
						if($data->Password == $data->Confirm)
						{
							$user->Password = md5($data->Password);

							$user->save();
							$this->_flash('alert', 'Seus dados foram atualizados com sucesso.');
						}
						else
						{
							$this->_flash('alert alert-error', 'Os campos "Senha" e "Confirmar Senha" não estão iguais.');
						}
					}
					else
					{
						$this->_flash('alert alert-error', 'Sua senha antiga está incorreta.');
					}
				}
				else
				{
					$this->_flash('alert alert-error', 'Você não foi encontrado! Isso mesmo, você não foi encontrado!');
				}
			} 
			catch (ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch (Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível salvar seus dados.');
			}
		}
		return $this->_view();
	}
	
	public function login()
	{
		if(Request::isPost())
		{
			$user = User::get_by_name($this->_data()->Username);
			if($user && $user->Password == md5($this->_data()->Password))
			{
				if($user->IsApproved)
				{
					Session::set('user', $user);
					Auth::set('user');
					
					$this->_redirect('~/component/my');
				}
				else
				{
					$this->_flash('alert', 'Por favor, confirme sua conta por e-mail.');
				}
			}
			else
			{
				$this->_flash('alert alert-error', 'Usuário ou senha incorretos.');
			}
		}
		return $this->_view();
	}
	
	/** @Auth("user") */
	public function logout()
	{
		Auth::clear();
		Session::clear();
		$this->_redirect('~/');
	}
}