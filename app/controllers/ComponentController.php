<?php
class ComponentController extends Controller
{	
	public function view($name, $version = 'last')
	{
		$component	= Component::get_by_name($name, $version);
		$versions	= Component::list_versions($name);
		$this->_set('versions', $versions);
		
		$count_total	= Download::count($component->Id);
		$this->_set('count_total', $count_total);
		
		$count_month	= Download::count_by_interval($component->Id, time() - MONTH, time());
		$this->_set('count_month', $count_month);
		
		$count_day		= Download::count_by_interval($component->Id, time() - DAY, time());
		$this->_set('count_day', $count_day);
		
		return $this->_view($component);
	}
	
	public function download($name, $version = 'last')
	{
		$component	= Component::get_by_name($name, $version);
		if($component)
		{
			try
			{
				$download = new Download();
				$download->ComponentId = $component->Id;
				$download->Date = time();
				$download->Origin = 0;
				$download->save();
			}
			catch(Exception $e)
			{
			}
			$this->_redirect('~/upload/zip/' . $component->Name . '-' . $component->Version . '.zip');
		}
		return $this->_snippet('notfound');
	}
	
	public function search($p = 1)
	{
		$query = $this->_args('q');
		$components = Component::search($p, 10, 'Version', 'DESC', $query);
		return $this->_view($components);
	}
	
	/** @Auth("user") */
	public function my($p = 1)
	{
		$components = Component::list_by_user(Session::get('user')->Id, $p, 10, 'Version', 'DESC');
		return $this->_view($components);
	}
	
	/** @Auth("user") */
	public function submit()
	{
		if(IS_POST)
		{
			if(isset($_FILES['File']['name']))
			{
				$file = $_FILES['File'];
				if(in_array($file['type'], array('application/octet-stream','application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed')) && strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)) == 'zip')
				{
					$zip = new ZipArchive();
					if($zip->open($file['tmp_name']))
					{
						$folder = $zip->statIndex(0);
						if(preg_match('#\/$#', $folder['name']))
						{
							$json = $zip->getFromName($folder['name'] . 'component.json');
							if($json !== false)
							{
								$metadata = json_decode($json);
								if(is_object($metadata))
								{
									if($metadata->name . '/' == $folder['name'])
									{
										if(!Component::get_by_name($metadata->name, $metadata->version))
										{
											$requireFail = false;
											foreach($metadata->requires as $r)
											{
												if(!Component::get_by_name($r->name, $r->version))
												{
													$requireFail = 'O componente "'. $r->name .'" não foi encontrato em nosso banco de dados.';
													break;
												}
											}
											
											if($requireFail === false)
											{
												$path = WWWROOT . 'upload/zip/' . $metadata->name . '-' . $metadata->version . '.zip';
												if(move_uploaded_file($file['tmp_name'], $path))
												{
													try
													{
														$image = null;
														if(isset($_FILES['Image']['name']))
														{
															$image =  $metadata->name . '-' . $metadata->version . '.jpg';
															$canvas = new canvas();
															$canvas->carrega($_FILES['Image']['tmp_name'])
																   ->redimensiona(210, 210, 'crop')
																   ->grava(WWWROOT . 'upload/img/' . $image);
														}

														$component = new Component();
														$component->Name		= utf8_decode($metadata->name);
														$component->Title		= utf8_decode($metadata->title);
														$component->Description	= utf8_decode($metadata->description);
														$component->Version		= utf8_decode($metadata->version);
														$component->CreateDate	= time();
														$component->CreatorId	= Session::get('user')->Id;
														$component->Authors		= utf8_decode(json_encode($metadata->authors));
														$component->Requires	= utf8_decode(json_encode($metadata->requires));
														$component->Image		= $image;

														$component->save();
														
														foreach($metadata->requires as $r)
														{
															$req = new Required();
															$req->ComponentId	= (int)$component->Id;
															$req->RequiredName	= $r->name;
															$req->Version		= $r->version;
															$req->save();
														}

														$this->_flash('alert', 'Seu componente foi publicado.');
													}
													catch(Exception $e)
													{
														unlink($path);
														$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível publicar o componente.');
													}
												}
												else
												{
													$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar salvar o arquivo no servidor.');
												}
											}
											else
											{
												$this->_flash('alert alert-error', $requireFail);
											}
										}
										else 
										{
											$this->_flash('alert alert-error', 'Já existe um componente com este nome e versão.');
										}
									}
									else
									{
										$this->_flash('alert alert-error', 'O nome do componente está diferente do nome diretório compactado.');
									}
								}
								else 
								{
									$this->_flash('alert alert-error', 'Não foi possível ler o arquivo "component.json", certifique-se de que está correto.');
								}
							}
							else
							{
								$this->_flash('alert alert-error', 'Não foi possível encontrar o arquivo "component.json" dentro do zip.');
							}
						}
						else
						{
							$this->_flash('alert alert-error', 'O arquivo compactado deve conter o diretório do componente.');
						}
					}
					else 
					{
						$this->_flash('alert alert-error', 'Ocorreu um erro e não possível abrir o arquivo.');
					}
					$zip->close();
				}
				else
				{
					$this->_flash('alert alert-error', 'O arquivo deve estar no formato "zip".');
				}
			}
			else
			{
				$this->_flash('alert alert-error', 'Por favor, selecione um arquivo.');
			}
		}
		
		return $this->_view();
	}
	
	public function user($username = null)
	{
		return $this->_view();
	}
}