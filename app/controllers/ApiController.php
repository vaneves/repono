<?php
class ApiController extends Controller
{
	
	public function check($name, $version = 'last')
	{
		$component	= Component::get_by_name($name, $version);
		if($component)
		{
			exit(json_encode($component));
		}
	}
	
	public function download($name, $version = 'last')
	{
		$component	= Component::get_by_name($name, $version);
		if($component)
		{
			try
			{
				$download = new Download();
				$download->ComponentId = $component->Id;
				$download->Date = time();
				$download->Origin = 1;
				$download->save();
			}
			catch(Exception $e)
			{
			}
			$file = WWWROOT . 'upload/zip/' . $component->Name . '-' . $component->Version . '.zip';
			if(file_exists($file))
				exit(file_get_contents($file));
		}
		exit('notfound');
	}
}